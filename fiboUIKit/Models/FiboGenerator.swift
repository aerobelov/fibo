//
//  fiboGenerator.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 13.03.2022.
//

import Foundation


class FiboGenerator: Fetchable {
    
    func fetch(from pair: [UInt16], count n:UInt16, completion: @escaping (Result<[UInt16], GenError>) -> Void) {
        
        DispatchQueue.global().async {
            let one = pair[0]
            let two = pair[1]
            var currentPair = (one, two)
            var array: [UInt16] = []
            
            for _ in (1...n) {
                let num = currentPair.0 &+ currentPair.1
                array.append(num)
                currentPair = (currentPair.1, num)
            }
            DispatchQueue.main.sync {
                completion(Result.success(array))
            }
            
        }
        
    }
        
}
