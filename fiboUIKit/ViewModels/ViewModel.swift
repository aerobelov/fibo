//
//  viewModel.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 13.03.2022.
//

import Foundation

class ViewModel {
    
    var numbers: [UInt16] = []
    var currentTuple: [UInt16] = [0,1]
    var pageCount: UInt16 = 40
    var generator: Fetchable?
    var delegate: Reloadable?
    
    init(i: UInt16) {
        self.numbers = []
        self.currentTuple = [0, 1]
        switch i {
            case 0: self.generator = FiboGenerator()
            case 1: self.generator = PrimeGenerator()
            default: break
        }
        fetch()
    }
    
    
    //FETCH MORE VALUES
    func fetch() {
        generator?.fetch(from: self.currentTuple, count: self.pageCount) { [weak self] result in
            switch result {
            case .success(let array):
                self?.numbers.append(contentsOf: array)
                let last = array.count - 1
                self?.currentTuple = [array[last-1], array[last]]
                self?.delegate?.reload()
            case .failure(_):
                self?.delegate?.showError()
            }
        }
    }
    
    
}
