//
//  Errors.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 22.03.2022.
//

import Foundation

enum GenError: Error {
    case FiboError
    case EasyError
}
