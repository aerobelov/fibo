//
//  TableViewCell.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 13.03.2022.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var left: UILabel!
    @IBOutlet weak var right: UILabel!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var rightView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configure(in row: Int, with value: [UInt16]) {
        let left = String(value[0])
        let right = value.count > 1 ? String(value[1]) : ""
        if row.isMultiple(of: 2) {
            self.leftView.backgroundColor = UIColor.systemGray
            self.rightView.backgroundColor = UIColor.systemGray3
        } else {
            self.leftView.backgroundColor = UIColor.systemGray3
            self.rightView.backgroundColor = UIColor.systemGray
        }
        self.left.text = left
        self.right.text = right
    }
    
}
