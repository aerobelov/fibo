//
//  fiboGenerator.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 13.03.2022.
//

import Foundation

class PrimeGenerator: Fetchable {
    
    var primes: [UInt16] = [1]
    
    func fetch(from pair: [UInt16], count n:UInt16, completion: @escaping (Result<[UInt16], GenError>) -> Void) {
        
        DispatchQueue.global().async {
            
            var isPrime = true
            var target = pair[1] + 1
            let final = self.primes.count + Int(n)
            while self.primes.count < final {
                isPrime = true
                
                //MAX DIVIDER
                let max = sqrt(Double(target))
               
                //GENERATE PRIME
                //IF NUMBER HAS DIVIDER AMONG A<SQRT(NUMBER) - IT IS NOT PRIME
                for prime in self.primes {
                    if prime <= UInt16(max) {
                        if prime != 1  && (target % UInt16(prime) == 0) && (prime != target) {
                            isPrime = false
                        }
                    }
                    
                }

                if target >= UInt16.max{ break }
                
                if (isPrime == true) {
                    self.primes.append(UInt16(target))
                }
                
                target = target &+ 1
                
            }
            
            DispatchQueue.main.sync {
                completion(Result.success(self.primes.suffix(Int(n))))
            }
            
        }
        
    }
        
}
