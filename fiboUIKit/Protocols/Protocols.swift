//
//  Protocols.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 15.03.2022.
//

import Foundation

protocol Fetchable {
    func fetch (from pair: [UInt16], count n:UInt16, completion: @escaping (Result<[UInt16], GenError>) -> Void)
}

protocol Reloadable {
    func reload()
    func showError()
}
