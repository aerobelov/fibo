//
//  TableViewController.swift
//  fiboUIKit
//
//  Created by Pavel Belov on 12.03.2022.
//

import UIKit

class FiboTableView: UIViewController, Reloadable {
    
    @IBOutlet weak var fiboTable: UITableView!
    @IBOutlet weak var selection: UISegmentedControl!
    var model: ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.model = ViewModel(i: UInt16(self.selection.selectedSegmentIndex))
        self.model.delegate = self
        fiboTable.register(UINib(nibName: "TableViewCell", bundle: nil), forCellReuseIdentifier: "TableViewCell")
        fiboTable.dataSource = self
        fiboTable.delegate = self
        fiboTable.prefetchDataSource = self
    }
    
    func reload() {
        self.fiboTable.reloadData()
    }
    
    @IBAction func selected(sender: UISegmentedControl) {
        self.model = ViewModel(i: UInt16(sender.selectedSegmentIndex))
        self.model.delegate = self
        fiboTable.reloadData()
    }
    
}

extension FiboTableView: UITableViewDelegate {
    
}

extension FiboTableView {
    func showError() {
        let alert = UIAlertController(title: "Error", message: "Can not create number", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
}

extension FiboTableView: UITableViewDataSourcePrefetching {
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        
        if indexPaths.contains(where: isLoadingPage) {
            model.fetch()
            fiboTable.reloadData()
        }
    }
}

extension FiboTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.numbers.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = fiboTable.dequeueReusableCell(withIdentifier: "TableViewCell", for: indexPath) as! TableViewCell? {
            cell.configure(in: indexPath.row, with: model.numbers.chunked(into: 2)[indexPath.row])
            return cell
        }
        return UITableViewCell()
    }
    
}

private extension FiboTableView {
    func isLoadingPage(for indexPath: IndexPath) -> Bool {
        return (model.numbers.count/(indexPath.row+1)) < 3
    }
    
}
